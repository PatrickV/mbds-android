package messagerie.mbds.org.securemessage.models;

import java.io.Serializable;

public class Message implements Serializable {

    private int id;
    private String author;
    private String msg;
    private String dateCreated;
    private Boolean alreadyReturned;

    public Message() {

    }

    public Message (int id, String author, String msg, String dateCreated, Boolean alreadyReturned) {
        this.id = id;
        this.author = author;
        this.msg = msg;
        this.dateCreated = dateCreated;
        this.alreadyReturned = alreadyReturned;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Boolean getAlreadyReturned() {
        return alreadyReturned;
    }

    public void setAlreadyReturned(Boolean alreadyReturned) {
        this.alreadyReturned = alreadyReturned;
    }
}
