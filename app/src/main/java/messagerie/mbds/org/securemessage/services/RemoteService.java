package messagerie.mbds.org.securemessage.services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import messagerie.mbds.org.securemessage.config.Config;
import messagerie.mbds.org.securemessage.cryptages.Cryptage;
import messagerie.mbds.org.securemessage.database.Database;
import messagerie.mbds.org.securemessage.network.Sender;

public class RemoteService extends Service {

    private Handler mHandler;
    private String token = null;

    boolean firstRun = true;

    Date lastMessage;

    public static final long DEFAULT_SYNC_INTERVAL = 5 * 1000;
    public static final long LONG_SYNC_INTERVAL = 30 * 1000;

    public int delay = 5;

    RequestQueue queue = null;


    public RemoteService() {
    }

    private StringRequest generateStringRequestFetchMessages() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                Config.FETCH_MESSAGES_URL,
                new Response.Listener<String>() {

            @Override
            public void onResponse(final String response) {

                new Thread(new Runnable() {
                    public void run() {
                        try {
                            JSONArray messages = new JSONArray(response);
                            if (messages.length() > 0) {
                                if (firstRun) {
                                    for (int i = 0; i < messages.length(); i++) {
                                        JSONObject msg = messages.getJSONObject(i);
                                        Database data = Database.getINSTANCE(getApplicationContext());
                                        data.addMessage(msg.getString("author"), msg.getString("msg"), msg.getString("dateCreated"), msg.getBoolean("alreadyReturned"));
                                    }
                                    firstRun = false;
                                    lastMessage = Calendar.getInstance().getTime();
                                } else {
                                    for (int i = 0; i < messages.length(); i++) {
                                        JSONObject msg = messages.getJSONObject(i);

                                        if (!msg.getBoolean("alreadyReturned")) {
                                            Database data = Database.getINSTANCE(getApplicationContext());

                                            String message = "";

                                            if (msg.getString("msg").contains("[|]")) {
                                                String fullMessage = msg.getString("msg");
                                                String[] splitedMessage = fullMessage.split("[|]");
                                                Cryptage cryptage = new Cryptage("AndroidKeyStore", "RSA/ECB/PKCS1Padding", "toto");

                                                switch (splitedMessage[1]) {
                                                    case "PING":
                                                        String publicKey = splitedMessage[2];

                                                        String aes = cryptage.genererAES();
                                                        Sender.sendCommand(getApplicationContext(), splitedMessage[0], "PONG",cryptage.chiffrerAvecClePublic(publicKey, aes));
                                                        data.addAESKeyToContact(splitedMessage[0], aes);
                                                        break;
                                                    case "PONG":
                                                        data.addAESKeyToContact(splitedMessage[0], cryptage.dechiffrer(splitedMessage[2]));
                                                        break;
                                                    case "MSG":
                                                        message = splitedMessage[2];
                                                        break;
                                                    default:
                                                        break;
                                                }
                                            } else {
                                                message = msg.getString("msg");
                                            }
                                            data.addMessage(msg.getString("author"), message, msg.getString("dateCreated"), msg.getBoolean("alreadyReturned"));
                                            lastMessage = Calendar.getInstance().getTime();
                                        }
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Bearer " + token);
                return params;
            }
        };

        return stringRequest;
    }

    public void syncData () {
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());

        queue = new RequestQueue(cache, network);
        queue.start();
        queue.add(generateStringRequestFetchMessages());
    }

    private Runnable runnableService = new Runnable() {
        @Override
        public void run() {
            syncData();

            Date now = Calendar.getInstance().getTime();

            if (lastMessage == null) {
                lastMessage = Calendar.getInstance().getTime();
            }

            long diff = now.getTime() - lastMessage.getTime();

            if (TimeUnit.MILLISECONDS.toMinutes(diff) < 3) {
                mHandler.postDelayed(runnableService, DEFAULT_SYNC_INTERVAL);
            } else {
                if ((delay * 1000) < LONG_SYNC_INTERVAL) {
                    delay += 5;
                    mHandler.postDelayed(runnableService, delay * 1000);
                } else {
                    mHandler.postDelayed(runnableService, LONG_SYNC_INTERVAL);
                }
            }
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.token = intent.getStringExtra("token");
        mHandler = new Handler();
        mHandler.post(runnableService);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
