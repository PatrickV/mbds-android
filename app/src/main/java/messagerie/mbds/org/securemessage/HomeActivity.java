package messagerie.mbds.org.securemessage;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.FrameLayout;

import messagerie.mbds.org.securemessage.cryptages.Cryptage;
import messagerie.mbds.org.securemessage.database.Database;
import messagerie.mbds.org.securemessage.fragments.ContactFragment;
import messagerie.mbds.org.securemessage.fragments.MessageFragment;
import messagerie.mbds.org.securemessage.interfaces.FragmentInterface;
import messagerie.mbds.org.securemessage.models.Personne;

public class HomeActivity extends AppCompatActivity implements FragmentInterface {

    MessageFragment messageFragment = new MessageFragment();
    ContactFragment contactFragment = new ContactFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        messageFragment = new MessageFragment();
        contactFragment = new ContactFragment();

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("user_prefs", getApplicationContext().MODE_PRIVATE);
        String key = sharedPreferences.getString("SecretKey", null);

        Database db = Database.getINSTANCE(getApplicationContext());

        if (key == null || key.isEmpty()) {
            if (db.getUserScret(sharedPreferences.getString("username", null)) == null ||
                    db.getUserScret(sharedPreferences.getString("username", null)).isEmpty()) {
                Cryptage cryptage = new Cryptage("AndroidKeyStore", "RSA/ECB/PKCS1Padding", "toto");
                sharedPreferences.edit().putString("SecretKey", cryptage.genererAES()).apply();
                db.addUser(sharedPreferences.getString("username", null), sharedPreferences.getString("SecretKey", null));
            } else {
                sharedPreferences.edit().putString("SecretKey", db.getUserScret(sharedPreferences.getString("username", null))).apply();
            }
        }



        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();


        FrameLayout flContactHolder = findViewById(R.id.contact_holder);
        FrameLayout flMessageHolder = findViewById(R.id.message_holder);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (flMessageHolder != null) {
            ft.replace(flMessageHolder.getId(), messageFragment);
            ft.replace(flContactHolder.getId(), contactFragment);
        } else {
            ft.replace(flContactHolder.getId(), contactFragment);
        }
        ft.commit();

        if (Intent.ACTION_SEND.equals(action) && type != null){
            if ("text/plain".equals(type)) {
                handleSendText(intent); // Handle text being sent
            }
        }
    }

    @Override
    public void moveToContact() {
        FrameLayout flContactHolder = findViewById(R.id.contact_holder);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(flContactHolder.getId(), contactFragment);
        ft.commit();
    }

    @Override
    public void moveToMessage(Personne contact) {
        Bundle arguments = new Bundle();
        arguments.putSerializable("personne", contact);

        messageFragment.setArguments(arguments);

        FrameLayout flContactHolder =  findViewById(R.id.contact_holder);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(flContactHolder.getId(), messageFragment);
        ft.commit();
    }

    @Override
    public void moveToMessage(Personne contact, String message) {
        Bundle arguments = new Bundle();
        arguments.putSerializable("personne", contact);
        if (message != null && message != "") {
            arguments.putString("message", message);
        }

        messageFragment.setArguments(arguments);

        FrameLayout flContactHolder = findViewById(R.id.contact_holder);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(flContactHolder.getId(), messageFragment);
        ft.commit();
    }

    @Override
    public void changeMessageView(Personne contact, String message) {
        Bundle arguments = new Bundle();
        arguments.putSerializable("personne", contact);
        if (message != null && message != "") {
            arguments.putString("message", message);
        }

        messageFragment.setArguments(arguments);

        getSupportFragmentManager()
                .beginTransaction()
                .detach(messageFragment)
                .attach(messageFragment)
                .commit();

    }

    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            Bundle arguments = new Bundle();
            arguments.putString("message", sharedText);

            contactFragment.setArguments(arguments);
            FrameLayout flContactHolder = findViewById(R.id.contact_holder);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(flContactHolder.getId(), contactFragment);
            ft.commit();
        }
    }
}
