package messagerie.mbds.org.securemessage.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.List;

import messagerie.mbds.org.securemessage.cryptages.Cryptage;
import messagerie.mbds.org.securemessage.models.Message;
import messagerie.mbds.org.securemessage.models.Personne;

public class Database {

    private DatabaseHelper dbHelper;
    private static Database INSTANCE = null;
    private Context context;

    private Database (Context context) {
        this.context = context;
        dbHelper = new DatabaseHelper(context);
    }

    public static Database getINSTANCE(Context context) {
        return (INSTANCE == null) ? new Database(context) : INSTANCE;
    }

    public void addContact(String firstname, String lastname, String username) {
        // Gets the data repository in write mode
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        SharedPreferences sharedPreferences = context.getSharedPreferences("user_prefs", context.MODE_PRIVATE);

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(ContactContract.FeedContact.COLUMN_NAME_OWNER, sharedPreferences.getString("username", null));
        values.put(ContactContract.FeedContact.COLUMN_NAME_LASTNAME, firstname);
        values.put(ContactContract.FeedContact.COLUMN_NAME_FIRSTNAME, lastname);
        values.put(ContactContract.FeedContact.COLUMN_NAME_USERNAME, username);
        values.put(ContactContract.FeedContact.COLUMN_NAME_KEY, "");

        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(ContactContract.FeedContact.TABLE_NAME, null, values);
    }

    public void addUser(String username, String secret) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(UserContract.FeedUser.COLUMN_NAME_USERNAME, username);
        values.put(UserContract.FeedUser.COLUMN_NAME_SECRET, secret);

        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(UserContract.FeedUser.TABLE_NAME, null, values);
    }

    public void addMessage(String author, String message, String date, Boolean returned) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cryptage cryptage = new Cryptage("AndroidKeyStore", "RSA/ECB/PKCS1Padding", "toto");

        SharedPreferences sharedPreferences = context.getSharedPreferences("user_prefs", context.MODE_PRIVATE);
        final String user = sharedPreferences.getString("username", null);

        ContentValues values = new ContentValues();
        values.put(MessageContract.FeedMessage.COLUMN_NAME_AUTHOR, author);
        values.put(MessageContract.FeedMessage.COLUMN_NAME_MSG, cryptage.chiffrerAES(message, sharedPreferences.getString("SecretKey", null)));
        values.put(MessageContract.FeedMessage.COLUMN_NAME_DATE, date);
        int booleanReturn = (returned) ? 1 : 0;
        values.put(MessageContract.FeedMessage.COLUMN_NAME_RETURNED, booleanReturn);

        long newRowId = db.insert(MessageContract.FeedMessage.TABLE_NAME, null, values);
    }

    public void addMessageLocal(String author, String message, String target) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        SharedPreferences sharedPreferences = context.getSharedPreferences("user_prefs", context.MODE_PRIVATE);
        final String user = sharedPreferences.getString("username", null);

        String aes = "";
        Cryptage cryptage = new Cryptage();
        ContentValues values = new ContentValues();

        if (author.compareToIgnoreCase(user) != 0) {
            aes = getContactAes(author);
            values.put(MessageContract.FeedMessage.COLUMN_NAME_MSG, cryptage.chiffrerAES(message, aes));
        } else {
            values.put(MessageContract.FeedMessage.COLUMN_NAME_MSG, message);
        }

        values.put(MessageContract.FeedMessage.COLUMN_NAME_AUTHOR, author);
        values.put(MessageContract.FeedMessage.COLUMN_NAME_TARGET, target);

        long newRowId = db.insert(MessageContract.FeedMessage.TABLE_NAME, null, values);
    }

    public boolean alreadyIn(String author, String message, String date) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cryptage cryptage = new Cryptage("AndroidKeyStore", "RSA/ECB/PKCS1Padding", "toto");

        SharedPreferences sharedPreferences = context.getSharedPreferences("user_prefs", context.MODE_PRIVATE);

        Cursor cursor = db.rawQuery("select * " +
                "from Message " +
                "where author = ? and msg = ? and dateCreated = ?",
                new String[]{author, cryptage.chiffrerAES(message, sharedPreferences.getString("SecretKey", null)), date});

        if (cursor.moveToFirst()) {
            cursor.close();
            return true;
        }

        cursor.close();
        return false;
    }

    public void addMessageToQueue(String target, String message) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        SharedPreferences sharedPreferences = context.getSharedPreferences("user_prefs", context.MODE_PRIVATE);
        final String user = sharedPreferences.getString("username", null);

        ContentValues values = new ContentValues();
        values.put(BufferContract.FeedBuffer.COLUMN_NAME_MSG, message);
        values.put(BufferContract.FeedBuffer.COLUMN_NAME_AUTHOR, user);
        values.put(BufferContract.FeedBuffer.COLUMN_NAME_TARGET, target);
        values.put(BufferContract.FeedBuffer.COLUMN_NAME_SENT, 0);

        db.insert(BufferContract.FeedBuffer.TABLE_NAME, null, values);
    }

    public ArrayList<String> messagesInQueue(String target) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<String> messages = new ArrayList<>();

        Cursor cursor = db.rawQuery("select * from buffer where target = ?", new String[]{target});
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String message = cursor.getString(cursor.getColumnIndex("Message"));
                messages.add(message);
                cursor.moveToNext();
            }
        }

        return messages;
    }

    public String getContactUserName(String firstname, String lastname) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String username = "";

        Cursor cursor = db.rawQuery("select * from contact where firstname = ? and lastname = ?", new String[]{firstname, lastname});

        if (cursor != null) {
            cursor.moveToFirst();
            username = cursor.getString(cursor.getColumnIndex("Username"));
        }

        cursor.close();

        return username;
    }

    public String getContactAes(String username) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String aes = null;

        Cursor cursor = db.rawQuery("select * from contact where username = ?", new String[]{username});

        if (cursor.moveToFirst()) {
            aes = cursor.getString(cursor.getColumnIndex("SecretKey"));
        }

        cursor.close();

        return aes;
    }

    public String getUserScret(String username) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String aes = null;

        Cursor cursor = db.rawQuery("select * from user where username = ?", new String[]{username});

        if (cursor.moveToFirst()) {
            aes = cursor.getString(cursor.getColumnIndex("SecretKey"));
        }

        cursor.close();

        return aes;
    }

    public boolean addAESKeyToContact(String username, String aes) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("SecretKey", aes);

        return db.update(ContactContract.FeedContact.TABLE_NAME, cv, "Username=" + username, null) == 1 ? true : false;
    }

    public List<Personne> readPersonnes() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] projection = {
                BaseColumns._ID,
                ContactContract.FeedContact.COLUMN_NAME_LASTNAME,
                ContactContract.FeedContact.COLUMN_NAME_FIRSTNAME
        };


        String selection = "";
        String[] selectionArgs = null;

        String sortOrder = ContactContract.FeedContact.COLUMN_NAME_LASTNAME + " DESC";

        Cursor cursor = db.query(
                ContactContract.FeedContact.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );

        List persons = new ArrayList<Personne>();
        while(cursor.moveToNext()) {
            long itemId = cursor.getLong(cursor.getColumnIndexOrThrow(ContactContract.FeedContact._ID));
            String nom = cursor.getString(cursor.getColumnIndex(ContactContract.FeedContact.COLUMN_NAME_LASTNAME));
            String prenom = cursor.getString(cursor.getColumnIndex(ContactContract.FeedContact.COLUMN_NAME_FIRSTNAME));
            persons.add(new Personne(nom,prenom));
        }

        cursor.close();

        return persons;
    }

    public List<Message> readMessages() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] projection = {
                BaseColumns._ID,
                MessageContract.FeedMessage.COLUMN_NAME_AUTHOR,
                MessageContract.FeedMessage.COLUMN_NAME_MSG,
                MessageContract.FeedMessage.COLUMN_NAME_DATE,
                MessageContract.FeedMessage.COLUMN_NAME_RETURNED
        };

        String selection = "";
        String[] selectionArgs = null;

        String sortOrder = MessageContract.FeedMessage.COLUMN_NAME_DATE + " DESC";

        Cursor cursor = db.query(
                MessageContract.FeedMessage.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );

        List messages = new ArrayList<Message>();
        while (cursor.moveToNext()) {
            long itemId = cursor.getLong(cursor.getColumnIndexOrThrow(MessageContract.FeedMessage._ID));
            String author = cursor.getString(cursor.getColumnIndexOrThrow(MessageContract.FeedMessage.COLUMN_NAME_AUTHOR));
            String message = cursor.getString(cursor.getColumnIndexOrThrow(MessageContract.FeedMessage.COLUMN_NAME_MSG));
            String date = cursor.getString(cursor.getColumnIndexOrThrow(MessageContract.FeedMessage.COLUMN_NAME_DATE));
            int booleanReturned = cursor.getInt(cursor.getColumnIndexOrThrow(MessageContract.FeedMessage.COLUMN_NAME_RETURNED));
            Boolean returned = (booleanReturned == 1) ? true : false;
            messages.add(new Message(Integer.parseInt(itemId + ""), author, message, date, returned));
        }

        cursor.close();
        return messages;
    }



    class ContactContract {

        private ContactContract() {

        }

        class FeedContact implements BaseColumns {
            static final String TABLE_NAME = "Contact";
            static final String COLUMN_NAME_LASTNAME = "Lastname";
            static final String COLUMN_NAME_FIRSTNAME = "Firstname";
            static final String COLUMN_NAME_OWNER = "Owner";
            static final String COLUMN_NAME_USERNAME = "Username";
            static final String COLUMN_NAME_KEY = "SecretKey";
        }
    }

    class UserContract {

        private UserContract() {

        }

        class FeedUser implements BaseColumns {
            static final String TABLE_NAME = "User";
            static final String COLUMN_NAME_USERNAME = "Username";
            static final String COLUMN_NAME_SECRET = "SecretKey";
        }
    }

    class MessageContract {
        private MessageContract() {

        }

        class FeedMessage implements BaseColumns {
            static final String TABLE_NAME = "Message";
            static final String COLUMN_NAME_AUTHOR = "Author";
            static final String COLUMN_NAME_TARGET = "Target";
            static final String COLUMN_NAME_MSG = "Msg";
            static final String COLUMN_NAME_DATE = "DateCreated";
            static final String COLUMN_NAME_RETURNED = "AlreadyReturned";
        }
    }

    class BufferContract {
        private BufferContract() {

        }

        class FeedBuffer implements BaseColumns {
            static final String TABLE_NAME = "Buffer";
            static final String COLUMN_NAME_AUTHOR = "Author";
            static final String COLUMN_NAME_TARGET = "Target";
            static final String COLUMN_NAME_MSG = "Message";
            static final String COLUMN_NAME_SENT = "Sent";
        }
    }
}
