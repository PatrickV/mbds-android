package messagerie.mbds.org.securemessage.cryptages;

import android.content.Context;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import messagerie.mbds.org.securemessage.database.Database;
import messagerie.mbds.org.securemessage.network.Sender;

public class Cryptage {

    private String provider;
    private String transformation;
    private KeyPair keyPair;
    private String alias;

    public Cryptage () {

        this("ANDROID_KEY_STORE", "RSA/ECB/PKCS1Padding", "toto");

        if (getPublicKey() == null) {
            createKeyPair();
        }
    }

    public Cryptage (String provider, String transformation, String alias) {
        this.provider = provider;
        this.transformation = transformation;
        this.alias = alias;
        if (getPublicKey() == null) {
            createKeyPair();
        }
    }

    public void createKeyPair() {
        KeyPairGenerator kpg = null;
        try {
            kpg = KeyPairGenerator.getInstance("RSA", provider);
            KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec.Builder(alias,
                    KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_ECB)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1);
            kpg.initialize(builder.build());
            keyPair = kpg.genKeyPair();
        } catch (NoSuchAlgorithmException
                | NoSuchProviderException
                | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
    }

    public PublicKey getPublicKey () {
        KeyStore keyStore = null;
        PublicKey publicKey = null;
        try {
            keyStore = KeyStore.getInstance(provider);
            keyStore.load(null);
            if (keyStore == null) {
                return null;
            }
            if (keyStore.getCertificate(alias) == null) {
                return null;
            }
            publicKey = keyStore.getCertificate(alias).getPublicKey();
        } catch (KeyStoreException
                | CertificateException
                | NoSuchAlgorithmException
                | IOException e) {
            e.printStackTrace();
        }
        return publicKey;
    }

    public PrivateKey getPrivateKey () {
        PrivateKey privateKey = null;

        KeyStore keyStore = null;
        try {
            keyStore = KeyStore.getInstance(provider);
            keyStore.load(null);
            KeyStore.Entry entry = keyStore.getEntry(alias, null);
            privateKey = ((KeyStore.PrivateKeyEntry) entry).getPrivateKey();
        } catch (KeyStoreException
                | CertificateException
                | NoSuchAlgorithmException
                | IOException
                | UnrecoverableEntryException e) {
            e.printStackTrace();
        }
        return privateKey;
    }

    public String chiffrer(String message) {
        Cipher cipher = null;
        byte[] encryptedBytes = null;
        try {
            cipher = Cipher.getInstance(transformation);
            cipher.init(Cipher.ENCRYPT_MODE, getPublicKey());
            encryptedBytes = cipher.doFinal(message.getBytes());
        } catch (NoSuchPaddingException
                | BadPaddingException
                | NoSuchAlgorithmException
                | IllegalBlockSizeException
                | InvalidKeyException e) {
            e.printStackTrace();
        }
        try {
            return new String(encryptedBytes, "ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return new String(encryptedBytes);
    }

    public String chiffrerAvecClePublic(String cle, String message) {
        Cipher cipher = null;
        byte[] encryptedBytes = null;
        try {
            cipher = Cipher.getInstance(transformation);

            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(cle.getBytes());
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey pubKey = keyFactory.generatePublic(keySpec);

            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            encryptedBytes = cipher.doFinal(message.getBytes());
        } catch (NoSuchPaddingException
                | BadPaddingException
                | NoSuchAlgorithmException
                | IllegalBlockSizeException
                | InvalidKeyException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        try {
            return new String(encryptedBytes, "ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return new String(encryptedBytes);
    }

    public String dechiffrer(String message) {
        Cipher cipher1 = null;
        byte[] decryptedBytes = null;
        try {
            cipher1 = Cipher.getInstance(transformation);
            cipher1.init(Cipher.DECRYPT_MODE, getPrivateKey());
            decryptedBytes = cipher1.doFinal(message.getBytes("ISO-8859-1"));
        } catch (NoSuchAlgorithmException
                | NoSuchPaddingException
                | BadPaddingException
                | IllegalBlockSizeException
                | InvalidKeyException
                | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return new String(decryptedBytes);
    }

    private String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(15);
        char tempChar;
        for (int i = 0; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    public String genererAES() {
        KeyGenerator kgen = null;
        String tempCle = null;
        try {
            byte[] keyStart = this.random().getBytes();
            kgen = KeyGenerator.getInstance("AES");
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            sr.setSeed(keyStart);
            kgen.init(128, sr); // 192 and 256 bits may not be available
            SecretKey skey = kgen.generateKey();
            byte[] key = skey.getEncoded();
            tempCle = new String (key, "ISO-8859-1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return tempCle;
    }

    public String chiffrerAES(String message, String cle) {
        Cipher cipher = null;
        byte[] encryptedBytes = null;
        SecretKeySpec skeySpec = null;
        try {
            skeySpec = new SecretKeySpec(cle.getBytes("ISO-8859-1"), "AES");
            cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            encryptedBytes = cipher.doFinal(message.getBytes("ISO-8859-1"));
            return new String(encryptedBytes, "ISO-8859-1");
        } catch (UnsupportedEncodingException
                | NoSuchPaddingException
                | NoSuchAlgorithmException
                | InvalidKeyException
                | BadPaddingException
                | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return new String(encryptedBytes);
    }

    public String dechiffrerAES(String message, String cle) {
        SecretKeySpec skeySpec = null;
        Cipher cipher = null;
        byte[] decrypted = null;
        try {
            skeySpec = new SecretKeySpec(cle.getBytes("ISO-8859-1"), "AES");
            cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            decrypted = cipher.doFinal(message.getBytes("ISO-8859-1"));
            return new String(decrypted,"ISO-8859-1" );
        } catch (NoSuchAlgorithmException
                | NoSuchPaddingException
                | InvalidKeyException
                | BadPaddingException
                | UnsupportedEncodingException
                | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return new String(decrypted);
    }

    public void sendAesKey(String receiver, String key, Context context) {
        String aes = this.genererAES();
        Sender.sendCommand(context, receiver, "PONG", this.chiffrerAvecClePublic(key, aes));
        Database db = Database.getINSTANCE(context);
        db.addAESKeyToContact(receiver, aes);
    }

    public void sendPublicKey(String receiver, Context context) {
        byte[] key = getPublicKey().getEncoded();
        Sender.sendCommand(context, receiver, "PING", new String(key));
    }
}
