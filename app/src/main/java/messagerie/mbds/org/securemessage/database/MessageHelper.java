package messagerie.mbds.org.securemessage.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MessageHelper extends SQLiteOpenHelper {

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Database.MessageContract.FeedMessage.TABLE_NAME + " (" +
                    Database.MessageContract.FeedMessage._ID + " INTEGER PRIMARY KEY," +
                    Database.MessageContract.FeedMessage.COLUMN_NAME_AUTHOR + " TEXT," +
                    Database.MessageContract.FeedMessage.COLUMN_NAME_MSG + " TEXT," +
                    Database.MessageContract.FeedMessage.COLUMN_NAME_DATE + " TEXT," +
                    Database.MessageContract.FeedMessage.COLUMN_NAME_RETURNED + " INTEGER)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Database.MessageContract.FeedMessage.TABLE_NAME;

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Messagerie.db";

    public MessageHelper (Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", "onCreate: Message");
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
