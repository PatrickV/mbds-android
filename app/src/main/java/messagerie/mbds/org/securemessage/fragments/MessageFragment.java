package messagerie.mbds.org.securemessage.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import messagerie.mbds.org.securemessage.R;
import messagerie.mbds.org.securemessage.cryptages.Cryptage;
import messagerie.mbds.org.securemessage.database.Database;
import messagerie.mbds.org.securemessage.interfaces.FragmentInterface;
import messagerie.mbds.org.securemessage.models.Personne;
import messagerie.mbds.org.securemessage.network.Sender;

public class MessageFragment extends android.support.v4.app.Fragment {

    FragmentInterface fragmentInterface;
    Bundle bundle;
    String username;
    List<String> mesMessages = new ArrayList<>();

    public MessageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentInterface = (FragmentInterface)context;
        } catch (Exception e) {
            throw new ClassCastException(context.toString());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_message, container, false);

        ImageButton imgButtonBack = (ImageButton) view.findViewById(R.id.return_to_contact);
        ImageButton imgButtonSend = (ImageButton) view.findViewById(R.id.send_message);
        TextView writeMessageTv = (TextView) view.findViewById(R.id.write_message);

        writeMessageTv.setHint(getString(R.string.write_message));

        final ListView listeMessages = (ListView) view.findViewById(R.id.messages_list);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, mesMessages);
        listeMessages.setAdapter(adapter);

        listeMessages.post(new Runnable() {
            @Override
            public void run() {
                listeMessages.setSelection(adapter.getCount() - 1);
            }
        });

        if (getArguments() != null) {
            this.bundle = getArguments();

            for (String key : this.bundle.keySet()) {
                if (key == "personne") {
                    Personne personne = (Personne) bundle.getSerializable("personne");

                    TextView header_message_tv = (TextView) view.findViewById(R.id.header_message_tv);
                    header_message_tv.setText(personne.getPrenom() + " " + personne.getNom());

                    Database db = Database.getINSTANCE(getContext());
                    username = db.getContactUserName(personne.getPrenom(), personne.getNom());

                    imgButtonBack.setVisibility(View.VISIBLE);
                    View separator = (View) view.findViewById(R.id.message_separator);
                    separator.setVisibility(View.INVISIBLE);
                } else if (key == "message") {
                    String message = bundle.getString("message");
                    mesMessages.add(message);
                    adapter.notifyDataSetChanged();
                }
            }
        } else {
            imgButtonBack.setVisibility(View.INVISIBLE);
        }

        imgButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentInterface.moveToContact();
            }
        });

        imgButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView writeMessageTvOnClick = (TextView) view.findViewById(R.id.write_message);
                String message = writeMessageTvOnClick.getText().toString();
                mesMessages.add(message);
                adapter.notifyDataSetChanged();

                Database db = Database.getINSTANCE(getContext());
                if (db.getContactAes(username) == null || db.getContactAes(username).isEmpty()) {
                    db.addMessageToQueue(username, message);
                } else {
                    Cryptage cryptage = new Cryptage("AndroidKeyStore", "RSA/ECB/PKCS1Padding", "toto");
                    String aes = db.getContactAes(username);

                    if (!db.messagesInQueue(username).isEmpty()) {
                        ArrayList<String> messages = db.messagesInQueue(username);
                        for (String msg : messages) {
                            Sender.sendCommand(getContext(), username, "MSG", cryptage.chiffrerAES(msg, aes));
                        }
                    }
                    Sender.sendCommand(getContext(), username, "MSG", cryptage.chiffrerAES(message, aes));
                }
            }
        });

        return view;
    }
}
