package messagerie.mbds.org.securemessage.network;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import messagerie.mbds.org.securemessage.R;
import messagerie.mbds.org.securemessage.config.Config;
import messagerie.mbds.org.securemessage.database.Database;

public abstract class Sender {

    public static void sendCommand(final Context context, final String receiver, final String command, final String message) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("user_prefs", context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token", null);
        final String sender = sharedPreferences.getString("username", null);

        RequestQueue queue = null;

        JSONObject messageObj = new JSONObject();
        try {
            messageObj.put("message", sender + "[|]" + command.toUpperCase() + "[|]" + message);
            messageObj.put("receiver", receiver);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Config.SEND_MSG_URL, messageObj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        switch (command) {
                            case "PING":
                                break;
                            case "PONG":
                                break;
                            case "MSG":
                                Database dbMsg = Database.getINSTANCE(context);
                                dbMsg.addMessageLocal(sender, message, receiver);
                                Toast toast = Toast.makeText(context, R.string.msg_sent, Toast.LENGTH_LONG);
                                toast.show();
                            default:
                                break;
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Volley error", "Error: " + error.getMessage());
                Toast toast = Toast.makeText(context, new String(error.networkResponse.data), Toast.LENGTH_LONG);
                toast.show();
            }
        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer " + token);
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };

        Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());

        queue = new RequestQueue(cache, network);
        queue.start();
        queue.add(jsonObjReq);
    }

}
