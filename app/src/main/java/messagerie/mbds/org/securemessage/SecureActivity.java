package messagerie.mbds.org.securemessage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import messagerie.mbds.org.securemessage.cryptages.Cryptage;

public class SecureActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secure);

        String tempCle = null;

        Button crypter = (Button) findViewById(R.id.chiffrer_boutton);
        Button decrypter = (Button) findViewById(R.id.dechiffrer_button);
        Button crypterAES = (Button) findViewById(R.id.chiffrer_boutton_aes);
        Button decrypterAES = (Button) findViewById(R.id.dechiffrer_button_aes);

        final EditText messageToCrypt = (EditText) findViewById(R.id.chiffrer_textview);
        final EditText messageToDecrypt = (EditText) findViewById(R.id.dechiffrer_textview);

        final EditText messageToCryptAES = (EditText) findViewById(R.id.chiffrer_textview_aes);
        final EditText messageToDecryptAES = (EditText) findViewById(R.id.dechiffrer_textview_aes);

        final Cryptage cryptage = new Cryptage("AndroidKeyStore", "RSA/ECB/PKCS1Padding", "toto");

        KeyGenerator kgen = null;
        try {
            byte[] keyStart = "this is a key".getBytes();
            kgen = KeyGenerator.getInstance("AES");
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            sr.setSeed(keyStart);
            kgen.init(128, sr); // 192 and 256 bits may not be available
            SecretKey skey = kgen.generateKey();
            byte[] key = skey.getEncoded();
            tempCle = new String (key, "ISO-8859-1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        final String cle = tempCle;

        crypter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messageToCrypt.getText().length() > 0) {
                    String message = messageToCrypt.getText().toString();
                    messageToDecrypt.setText(cryptage.chiffrer(message));
                    messageToCrypt.setText("");
                }
            }
        });

        decrypter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messageToDecrypt.getText().length() > 0) {
                    String message = messageToDecrypt.getText().toString();
                    messageToCrypt.setText(cryptage.dechiffrer(message));
                    messageToDecrypt.setText("");
                }
            }
        });

        crypterAES.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messageToCryptAES.getText().length() > 0) {
                    String message = messageToCryptAES.getText().toString();
                    messageToDecryptAES.setText(cryptage.chiffrerAES(message, cle));
                    messageToCryptAES.setText("");
                }
            }
        });

        decrypterAES.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messageToDecryptAES.getText().length() > 0) {
                    String message = messageToDecryptAES.getText().toString();
                    messageToCryptAES.setText(cryptage.dechiffrerAES(message, cle));
                    messageToDecryptAES.setText("");
                }
            }
        });
    }
}
