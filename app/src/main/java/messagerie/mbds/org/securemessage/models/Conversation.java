package messagerie.mbds.org.securemessage.models;

import java.io.Serializable;
import java.util.List;

public class Conversation implements Serializable {

    private String nom;
    private List<Personne> participants;
    private List<Message> messages;

    public Conversation (String nom, List<Personne> participants, List<Message> messages) {
        this.nom = nom;
        this.participants = participants;
        this.messages = messages;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Personne> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Personne> participants) {
        this.participants = participants;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
