package messagerie.mbds.org.securemessage.config;

public class Config {

    public static final String SERVEUR_BASE_URL = "http://baobab.tokidev.fr";
    public static final String LOGIN_URL = "http://baobab.tokidev.fr/api/login";
    public static final String CREATE_USER_URL = "http://baobab.tokidev.fr/api/createUser";
    public static final String SEND_MSG_URL = "http://baobab.tokidev.fr/api/sendMsg";
    public static final String FETCH_MESSAGES_URL = "http://baobab.tokidev.fr/api/fetchMessages";

}
