package messagerie.mbds.org.securemessage;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import messagerie.mbds.org.securemessage.config.Config;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,  WindowManager.LayoutParams.FLAG_FULLSCREEN);

        final EditText newPersonFirstname = (EditText) findViewById(R.id.new_person_firstname);
        final EditText newPersonLastname = (EditText) findViewById(R.id.new_person_lastname);
        final EditText newPersonUsername = (EditText) findViewById(R.id.new_person_username);
        final EditText newPersonPassword = (EditText) findViewById(R.id.new_person_password);

        final TextView alertRegister = (TextView) findViewById(R.id.alert_register_tv);

        Button createButton = (Button) findViewById(R.id.create_register_button);
        Button cancelButton = (Button) findViewById(R.id.cancel_register_button);

        newPersonFirstname.setHint(R.string.newFirstName);
        newPersonLastname.setHint(R.string.newLastnameText);
        newPersonUsername.setHint(R.string.newUsernameText);
        newPersonPassword.setHint(R.string.newPasswordText);

        if (getIntent().getStringExtra("message") != null) {
            String message = getIntent().getStringExtra("message");
            alertRegister.setTextColor(Color.RED);
            alertRegister.setText(message);
            alertRegister.setGravity(Gravity.CENTER);

            Timer t = new Timer(false);
            t.schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            alertRegister.setVisibility(View.INVISIBLE);
                        }
                    });
                }
            }, 5000);
        }

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterActivity.this.finish();
            }
        });

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RequestQueue queue = null;

                JSONObject message = new JSONObject();
                try {
                    message.put("username", newPersonUsername.getText().toString());
                    message.put("password", newPersonPassword.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                        Config.CREATE_USER_URL, message,
                        new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Context context = getApplicationContext();
                            CharSequence text = getString(R.string.account_created);
                            final int duration = Toast.LENGTH_LONG;

                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();

                            Thread thread = new Thread(){
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(duration);
                                        RegisterActivity.this.finish();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            };
                            thread.start();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyLog.d("Volley error", "Error: " + error.getMessage());

                            Context context = getApplicationContext();
                            CharSequence text = getString(R.string.account_created_error);
                            final int duration = Toast.LENGTH_LONG;

                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();

                            Thread thread = new Thread(){
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(duration);
                                        RegisterActivity.this.finish();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            };
                            thread.start();
                        }
                }) {
                    /**
                     * Passing some request headers
                     * */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        return headers;
                    }
                };

                Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork(new HurlStack());

                queue = new RequestQueue(cache, network);
                queue.start();
                queue.add(jsonObjReq);
            }
        });
    }
}
