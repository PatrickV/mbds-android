package messagerie.mbds.org.securemessage.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import messagerie.mbds.org.securemessage.R;
import messagerie.mbds.org.securemessage.interfaces.RecyclerViewClickListenerInterface;
import messagerie.mbds.org.securemessage.models.Personne;

public class ContactsPersonneAdapter extends RecyclerView.Adapter<ContactsPersonneAdapter.ViewHolder> {

    private List<Personne> mContacts;
    private RecyclerViewClickListenerInterface fragmentInterface;

    public ContactsPersonneAdapter(List<Personne> contacts, RecyclerViewClickListenerInterface fInterface) {
        this.mContacts = contacts;
        this.fragmentInterface = fInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, int i) {
        final int position = i;
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.contact_layout, viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        final Personne contact = mContacts.get(i);

        final String firstname = contact.getPrenom();
        final String lastname = contact.getNom();

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentInterface.recyclerViewListClicked(contact);
            }
        });
        TextView textViewFirstname = viewHolder.firstnameTextView;
        TextView textViewLastname = viewHolder.lastnameTextView;
        textViewFirstname.setText(firstname);
        textViewLastname.setText(lastname);
    }

    @Override
    public int getItemCount() {
        return mContacts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView firstnameTextView;
        public TextView lastnameTextView;
        public ViewHolder(View itemView) {
            super(itemView);
            firstnameTextView = (TextView) itemView.findViewById(R.id.personne_firstname);
            lastnameTextView = (TextView) itemView.findViewById(R.id.personne_lastname);
        }
    }
}
