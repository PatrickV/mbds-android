package messagerie.mbds.org.securemessage.interfaces;

import messagerie.mbds.org.securemessage.models.Personne;

public interface FragmentInterface {
    public void moveToContact();
    public void moveToMessage(Personne contact);
    public void moveToMessage(Personne contact, String message);
    public void changeMessageView(Personne contact, String message);
}
