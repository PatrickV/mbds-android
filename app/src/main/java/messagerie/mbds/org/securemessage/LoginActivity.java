package messagerie.mbds.org.securemessage;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.stetho.Stetho;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import messagerie.mbds.org.securemessage.config.Config;
import messagerie.mbds.org.securemessage.database.Database;
import messagerie.mbds.org.securemessage.services.RemoteService;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Database data = Database.getINSTANCE(getApplicationContext());

        Stetho.initialize(Stetho.newInitializerBuilder(this)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                .build());

        final EditText usernameET = (EditText) findViewById(R.id.login_login_tv);
        final EditText passwordET = (EditText) findViewById(R.id.password_login_tv);

        Button registerLoginButton = (Button) findViewById(R.id.register_login_button);
        Button loginButton = (Button) findViewById(R.id.login_login_button);

        usernameET.setHint(getResources().getString(R.string.loginText));
        passwordET.setHint(getResources().getString(R.string.passwordText));

        registerLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            }
        });

        Button secure = (Button) findViewById(R.id.button);

        secure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SecureActivity.class));
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String username = usernameET.getText().toString();
                final String password = passwordET.getText().toString();

                RequestQueue queue = null;

                JSONObject loginObj = new JSONObject();
                try {
                    loginObj.put("username", username);
                    loginObj.put("password", password);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                        Config.LOGIN_URL, loginObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("user_prefs", getApplicationContext().MODE_PRIVATE);
                                sharedPreferences.edit().putString("username", username).apply();
                                try {
                                    sharedPreferences.edit().putString("token", response.getString("access_token")).apply();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                Intent service = new Intent(getApplicationContext(), RemoteService.class);
                                service.putExtra("token", sharedPreferences.getString("token",null));
                                startService(service);

                                startActivity(new Intent(getApplicationContext(), HomeActivity.class));

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Volley error", "Error: " + error.getMessage());

                        Context context = getApplicationContext();
                        CharSequence text = getString(R.string.account_created_error);
                        final int duration = Toast.LENGTH_LONG;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();

                        Thread thread = new Thread(){
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(duration);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        };
                        thread.start();
                    }
                }) {
                    /**
                     * Passing some request headers
                     * */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        return headers;
                    }
                };

                Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork(new HurlStack());

                queue = new RequestQueue(cache, network);
                queue.start();
                queue.add(jsonObjReq);
            }
        });
    }
}
