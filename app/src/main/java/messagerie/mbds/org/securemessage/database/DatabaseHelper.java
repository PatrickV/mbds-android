package messagerie.mbds.org.securemessage.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Messagerie.db";

    /**
     *  CREATE TABLE CONTACT
     */
    private static final String SQL_CREATE_ENTRIES_CONTACT =
            "CREATE TABLE " + Database.ContactContract.FeedContact.TABLE_NAME + " (" +
                    Database.ContactContract.FeedContact._ID + " INTEGER PRIMARY KEY," +
                    Database.ContactContract.FeedContact.COLUMN_NAME_LASTNAME + " TEXT," +
                    Database.ContactContract.FeedContact.COLUMN_NAME_FIRSTNAME + " TEXT," +
                    Database.ContactContract.FeedContact.COLUMN_NAME_OWNER + " TEXT," +
                    Database.ContactContract.FeedContact.COLUMN_NAME_KEY + " TEXT," +
                    Database.ContactContract.FeedContact.COLUMN_NAME_USERNAME + " TEXT)";

    /**
     *  DROP TABLE CONTACT
     */
    private static final String SQL_DELETE_ENTRIES_CONTACT =
            "DROP TABLE IF EXISTS " + Database.ContactContract.FeedContact.TABLE_NAME;

    /**
     *  CREATE TABLE USER
     */
    private static final String SQL_CREATE_ENTRIES_USER =
            "CREATE TABLE " + Database.UserContract.FeedUser.TABLE_NAME + " (" +
                    Database.UserContract.FeedUser._ID + " INTEGER PRIMARY KEY," +
                    Database.UserContract.FeedUser.COLUMN_NAME_USERNAME + " TEXT," +
                    Database.UserContract.FeedUser.COLUMN_NAME_SECRET + " TEXT)";

    /**
     *  DROP TABLE USER
     */
    private static final String SQL_DELETE_ENTRIES_USER =
            "DROP TABLE IF EXISTS " + Database.UserContract.FeedUser.TABLE_NAME;

    /**
     *  CREATE TABLE MESSAGE
     */
    private static final String SQL_CREATE_ENTRIES_MESSAGE =
            "CREATE TABLE " + Database.MessageContract.FeedMessage.TABLE_NAME + " (" +
                    Database.MessageContract.FeedMessage._ID + " INTEGER PRIMARY KEY," +
                    Database.MessageContract.FeedMessage.COLUMN_NAME_AUTHOR + " TEXT," +
                    Database.MessageContract.FeedMessage.COLUMN_NAME_TARGET + " TEXT," +
                    Database.MessageContract.FeedMessage.COLUMN_NAME_MSG + " TEXT," +
                    Database.MessageContract.FeedMessage.COLUMN_NAME_DATE + " TEXT," +
                    Database.MessageContract.FeedMessage.COLUMN_NAME_RETURNED + " INTEGER)";

    /**
     *  DROP TABLE MESSAGE
     */
    private static final String SQL_DELETE_ENTRIES_MESSAGE =
            "DROP TABLE IF EXISTS " + Database.MessageContract.FeedMessage.TABLE_NAME;

    /**
     *  CREATE TABLE BUFFER
     */
    private static final String SQL_CREATE_ENTRIES_BUFFER =
            "CREATE TABLE " + Database.BufferContract.FeedBuffer.TABLE_NAME + " (" +
                    Database.BufferContract.FeedBuffer._ID + " INTEGER PRIMARY KEY," +
                    Database.BufferContract.FeedBuffer.COLUMN_NAME_AUTHOR + " TEXT," +
                    Database.BufferContract.FeedBuffer.COLUMN_NAME_TARGET + " TEXT," +
                    Database.BufferContract.FeedBuffer.COLUMN_NAME_MSG + " TEXT," +
                    Database.BufferContract.FeedBuffer.COLUMN_NAME_SENT + " INTEGER)";

    /**
     *  DROP TABLE BUFFER
     */
    private static final String SQL_DELETE_ENTRIES_BUFFER =
            "DROP TABLE IF EXISTS " + Database.BufferContract.FeedBuffer.TABLE_NAME;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES_CONTACT);
        db.execSQL(SQL_CREATE_ENTRIES_MESSAGE);
        db.execSQL(SQL_CREATE_ENTRIES_BUFFER);
        db.execSQL(SQL_CREATE_ENTRIES_USER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES_CONTACT);
        db.execSQL(SQL_DELETE_ENTRIES_MESSAGE);
        db.execSQL(SQL_DELETE_ENTRIES_BUFFER);
        db.execSQL(SQL_DELETE_ENTRIES_USER);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
