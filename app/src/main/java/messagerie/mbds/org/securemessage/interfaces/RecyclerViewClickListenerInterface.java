package messagerie.mbds.org.securemessage.interfaces;

import messagerie.mbds.org.securemessage.models.Personne;

public interface RecyclerViewClickListenerInterface {
    public void recyclerViewListClicked(final Personne contact);
}
