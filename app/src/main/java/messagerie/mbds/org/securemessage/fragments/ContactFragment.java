package messagerie.mbds.org.securemessage.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.List;

import messagerie.mbds.org.securemessage.R;
import messagerie.mbds.org.securemessage.adapters.ContactsPersonneAdapter;
import messagerie.mbds.org.securemessage.cryptages.Cryptage;
import messagerie.mbds.org.securemessage.database.Database;
import messagerie.mbds.org.securemessage.interfaces.FragmentInterface;
import messagerie.mbds.org.securemessage.interfaces.RecyclerViewClickListenerInterface;
import messagerie.mbds.org.securemessage.models.Personne;


public class ContactFragment extends android.support.v4.app.Fragment implements RecyclerViewClickListenerInterface {

    FragmentInterface fragmentInterface;
    String message = "";
    Context context;

    public ContactFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        try {
            fragmentInterface = (FragmentInterface) context;
        } catch (Exception e) {
            throw new ClassCastException(context.toString());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        message = "";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_contact, container, false);
        message = "";

        if (getArguments() != null) {
            if (getArguments().getString("message") != null) {
                message = getArguments().getString("message");
            }
        }

        populateList(view);

        ImageButton addContactButton = (ImageButton) view.findViewById(R.id.addContact);

        addContactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.add_contact, null);
                final EditText firstnameEditText = (EditText) dialogView.findViewById(R.id.contact_firstname);
                final EditText lastnameEditText = (EditText) dialogView.findViewById(R.id.contact_lastname);
                final EditText usernameEditText = (EditText) dialogView.findViewById(R.id.contact_username);
                builder.setView(dialogView)
                        .setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Database db = Database.getINSTANCE(context);
                                db.addContact(firstnameEditText.getText().toString(), lastnameEditText.getText().toString(), usernameEditText.getText().toString());
                                populateList(view);
                                dialog.dismiss();
                                Cryptage cryptage = new Cryptage("AndroidKeyStore", "RSA/ECB/PKCS1Padding", "toto");
                                cryptage.sendPublicKey(usernameEditText.getText().toString(), getContext());
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                builder.show();
            }
        });

        return view;
    }

    @Override
    public void recyclerViewListClicked(final Personne contact) {
        int currentOrientation = getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            fragmentInterface.changeMessageView(contact, message);
        } else {
            if (message != "") {
                fragmentInterface.moveToMessage(contact, message);
            } else {
                fragmentInterface.moveToMessage(contact);
            }
        }
    }

    private void populateList(View v) {
        RecyclerView listeContacts = (RecyclerView) v.findViewById(R.id.contacts_list);

        Database data = Database.getINSTANCE(getContext());
        List<Personne> listePersonnes = data.readPersonnes();

        ContactsPersonneAdapter contactsAdapter = new ContactsPersonneAdapter(listePersonnes, this);
        listeContacts.setHasFixedSize(true);
        listeContacts.setAdapter(contactsAdapter);
        listeContacts.setLayoutManager(new LinearLayoutManager(getActivity()));
    }
}
